# Theming a device

## Grub2 bootloader (grub.yml)

This playbook will copy grub2 themes from a central location (the files/grub2/themes subdirectory) to the target device.

To be on the safe side, it then crates a local copy of the /etc/default/grub configuration before it changes the GRUB_THEME
entry on the remote host (Note: You'll need to change this to match the theme you want to use)

In the final step the boot config on the remote host is written.

***

## SDDM Login Screen (sddm.yml)

This playbook will install the Sugar Candy (or another theme with slight modifications) SDDM theme on the target system. 

The first task copies the SDDM theme files form a central location (the files/sddm/themes subdirectory to the target device.

The second task creates a custom config for Sugar Candy adding the name of the target system to the login screen.

In the next step the configuration for sddm (from files/etc/sddm.conf.d/10-theme.conf) will be copied to the target device. If you want 
to use a different theme, you need to change this file.

In the final two steps a login screen image per server will be copied to the target device. This is done for two different themes 
(sugar-candy and sweet). 

***

## Distribute server pictures (pic.yml)

This is a trivial playbook. It just takes all the (picture) files in a directory and copies them to the target system. In my environment, there is
one file per node on my private LAN and these are used to graphically represent the nodes. 

