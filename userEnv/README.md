# Setup user environment

## Populate environment for user on (new) device

This playbook copies files and sets required configuration entries on a target device.

Note: I run this playbook for my account. Therefore it is acceptable that privte keys are inlcuded.
This is <<not>> something you should do in a multi-user setup! Other parts of the playbook might 
still be applicalbe if you are running this on behalf of other users.

This playbook performs the following steps:

- Copy customized files to target system and ensures correct ownership
- Create a symlink for the avatar (e.g. used on Login screens)
- Make sure files in ~/bin are executable
- Set protections on several files
- Modifies config depending on device (here i3 config on i3Device)
