# Setting up printers

## Distribute PPD (PPDdist.yml)

Simple case: Printer type is not known to your system yet, but there is a valid PPD (e.g. from the manufacturer) that can be used.
This only needs to be copied to the right place, which is all the playbook does.

***

## Install printer driver RPMs (addRPM.yml)

This is a little more complex playbook to install (pre-downloaded) printer driver RPMs on the target host. addRPM.yml mostly is a shell with a 
list of RPMs to install. These are processed in a loop calling includes/instZypp.yml to first copy the rpm files to the target host and then install
them there using zypper (obviously if you are not using openSUSE, you'll need to replace this with your package manager of choice).
