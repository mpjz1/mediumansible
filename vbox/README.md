# Setup Virtualbox

## SW installation and user setup

This playbook performs the required steps before you can start creating VMs on your systems with Virtualbox

- Install Virtualbox Software
- Add users who are allowed to control VMs to vboxusers group
- Perform basic vbox configuration on target device (if needed)
- Copy the Oracle extension pack (needs to be downloaded from Oracle Website) to target device
- Install Oracle extension pack
