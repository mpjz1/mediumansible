# Medium Ansible :-)

Ansible is a great tool to automate (system configuration) tasks you need to perform consistently on multiple systems or repeatedly on one.
There are many examples of Ansible playbooks in the documentation, but many of these tend to be on the pretty basic side.

Medium Ansible shares some real-world playbooks I wrote for my private network. They are a little more advanced than the typical examples you find, but also not very complicated. 

Thats why it is called "medium" Ansible.

Enjoy,

Martin
