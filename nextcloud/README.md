# Installing Nextcloud

## Installing prerequistis and basic Nextcloud setup (setupNC.yml)

This playbook contains many plays, but the general flow ist not too complicated

- Install prerequisite SW packages (inlcuding mariadb as storage engine)
- Create a simple test home page for testing of the new Webserver instance
- Enable needed Apache Modules (php7, rewrite)
- Update MIME config to recognize .php files
- Copy content to Webserver
- Adapt vaious setting in php.ini to meet Nextcloud requirements
- Adapt MariaDB config to meet Nextcloud requirement
- Restart/enabler apache2 and mariadb
- If MariaDB new (detected by setting mode=initial in the playbook!), break here and secure installation
- Add user of phpMyAdmin to MariaDB
- Create nextcloud DB and and user in MariaDB
- Install nextcloud SW
- Run a script on the target host to make sure DB files use UTF8mb4
- Update Nextcloud config
- Copy and activate local theme for NextCloud
- Create a cron job to update files on NextCloud

